import React from 'react';
import { Container } from '@material-ui/core';
import Home from './components/Home';

function App() {
  return (
    <Container maxWidth="md">
      <h1>El otro Borges.</h1>
      {/* <p>Otro lugar donde hay material sobre Jorge Luis Borges.</p> */}
      <p>
        Creado como una necesidad personal de contar con un espacio para reescuchar la voz de Borges
        y sus cuentos relatados por la voz de PlumaYPapel.
      </p>
      <Home />
    </Container>
  );
}

export default App;

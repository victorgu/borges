import React from 'react'
import Bio from './Bio'

function Home() {
  return (
    <div>
      <ul>
        <li>Bio</li>
        <li>Cuentos</li>
        <li>Entrevistas</li>
        <li>Conferencias</li>
        <li>Artículos</li>
        <li>Otros</li>
      </ul>
      <Bio />
    </div>
  )
}

export default Home

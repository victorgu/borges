import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import { Card } from 'react-rainbow-components'

const useStyles = makeStyles({
  avatar: {
    margin: 10,
  },
  bigAvatar: {
    margin: 10,
    width: 60,
    height: 60,
  },
});

function Bio() {
  const classes = useStyles();

  return (
    <Grid container justify="center" alignItems="center">
      <Card>
        <Avatar alt="Jorge Luis Borges" src="../images/jlb.jpg" className={classes.avatar} />
        {/* <Avatar alt="Jorge Luis Borges" src="../images/jlb.jpg" className={classes.bigAvatar} /> */}
        <p>Jorge Francisco Isidoro Luis Borges Acevedo (Buenos Aires, 24 de agosto de 1899-Ginebra, 14 de junio de 1986)
fue un erudito escritor argentino, considerado uno de los más destacados de la literatura del siglo XX.
Publicó ensayos breves, cuentos y poemas. Su obra, fundamental en la literatura y el pensamiento universales,
además de objeto de minuciosos análisis y múltiples interpretaciones, excluye todo dogmatismo.</p>
      </Card>
    </Grid>
        
  )
}

export default Bio  